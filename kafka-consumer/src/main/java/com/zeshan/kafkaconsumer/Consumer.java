package com.zeshan.kafkaconsumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;


@Component
public class Consumer {

    private static Logger logger = LoggerFactory.getLogger(Consumer.class);

    @KafkaListener(topics = "demo")
    public void listen(ConsumerRecord<?,?> record){
        logger.info("主题:{} 内容：{}",record.topic(),record.value());
    }
}
