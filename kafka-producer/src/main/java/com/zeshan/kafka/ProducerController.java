package com.zeshan.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("kafka-producer")
public class ProducerController {
    @Autowired
    private  KafkaTemplate kafkaTemplate;
    @RequestMapping("send")
    @ResponseBody
    public String  sengMessage(HttpServletRequest request, HttpServletResponse response){
        String message = request.getParameter("message");
        try {
            kafkaTemplate.send("demo",message);
            return "success";
        }catch (Exception e){
            e.printStackTrace();
            return  "error";
        }
    }
}
